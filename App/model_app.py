from ScoreCard.App.Analyze_feature import fea_train, data_analyze
from ScoreCard.Service.service import *
import os
import datetime
import pandas as pd
import warnings
import numpy as np
warnings.filterwarnings('ignore')
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from imblearn.over_sampling import SMOTE
import lightgbm as lgb
from sklearn.model_selection import learning_curve
from scipy import stats
from sklearn.model_selection import KFold, StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import roc_auc_score,accuracy_score
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.model_selection import train_test_split


fea_train = fea_train
data_analyze = data_analyze

# oversampling for inbalance dataset
data_analyze = data_analyze[fea_train + ['label']]
data_analyze = data_analyze[~data_analyze.isin([np.nan, np.inf, -np.inf]).any(1)]
X = data_analyze.drop('label', axis=1)
y = data_analyze['label']
col_names = pd.concat([X, y], axis=1).columns.tolist()
smt = SMOTE(random_state=100)
X_smote, y_smote = smt.fit_sample(X, y)
smote_array = np.concatenate([X_smote, y_smote.values.reshape(-1, 1)], axis=1)
data_analyze_smote = pd.DataFrame(smote_array, columns=col_names)

print(X_smote.shape, data_analyze.shape)

# train model with LightGBM
X_train, X_valid, y_train, y_valid = train_test_split(data_analyze_smote[[x for x in df_.columns if x != 'label']],
                                                      data_analyze_smote['label'],
                                                      test_size=0.1, random_state=12)

clf = lgb.LGBMClassifier(max_depth=100, random_state=123, silent=True, metric='AUC', n_jobs=30, n_estimators=7000,
                         num_leaves=200, num_iterations=7000, learning_rate=0.0015

clf.fit(X_train, y_train)

# check performance model
# Learning Curve
print('================================================')
print('Check Learning Curve')
train_sizes, train_scores, test_scores = learning_curve(clf, X_train, y_train, cv=3, n_jobs=1,
                                                        train_sizes=np.linspace(0.1, 1.0, 5))
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)
plt.grid()

plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                 train_scores_mean + train_scores_std, alpha=0.3,
                 color="r")
plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                 test_scores_mean + test_scores_std, alpha=0.3,
                 color="g")
plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
         label="Training score")
plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
         label="Cross-validation score")

plt.legend(loc="best")
# Check predict X_valid
print('================================================')

y_pred_test_over = clf.predict(X_valid)
i = 0
a = 0
for x in y_pred_test_over:
    if
x == 1.0:
i += 1
else:
a += 1
print('Good: %s' % i, 'Bad: %s' % a)
print('================================================')

# Calculate KS, Accuracy, Gini, AUC
y_pred_proba = clf.predict_proba(X_valid)

cmd_BAD, cmd_GOOD, thresholds = _KM(y_pred_proba[:, 1], n_bins=10)
auc = roc_auc_score(y_valid, clf.predict_proba(X_valid)[:, 1])
acc_test = accuracy_score(y_pred_test_over, y_valid)
print('KS: %s' % stats.ks_2samp(cmd_BAD, cmd_GOOD)[0])
print('AUC: %s' % auc)
print('Gini: %s' % (2 * auc - 1))
print('accuracy on test: ', acc_test)

arr = {'predict': list(y_pred_test_over), 'check_data': list(y_valid)}

g_b_df = pd.DataFrame(arr)
g_b_df.loc[g_b_df['check_data'] == 'good', 'check_data'] = 1
g_b_df.loc[g_b_df['check_data'] == 'bad', 'check_data'] = 0
g_b_df = g_b_df.astype(int)
g_b_df.loc[g_b_df.check_data == g_b_df.predict, 'check'] = 'True'
g_b_df.loc[g_b_df.check_data != g_b_df.predict, 'check'] = 'False'

print('predict_correct_bad: ' + str(
    len(g_b_df[(g_b_df.predict == 0) & (g_b_df.check == 'True')]) / len(g_b_df[(g_b_df.predict == 0)]) * 100))
print('predict_correct_good: ' + str(
    len(g_b_df[(g_b_df.predict == 1) & (g_b_df.check == 'True')]) / len(g_b_df[(g_b_df.predict == 1)]) * 100))
