from ScoreCard.Service.service import *
import os
import datetime
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import numpy as np
import math
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.model_selection import train_test_split


# process data for analyze feature
BAD_DPD_MAX = BAD_DPD_MAX
dt_train_frm = dt_train_frm
dt_train_to = dt_train_to
max_emi = max_emi

data_analyze_lms = process_data_lms(BAD_DPD_MAX, dt_train_frm, dt_train_to, max_emi)
data_analyze_los = process_data_los()
data_analyze_lstd = process_lstd(dt_train_frm, dt_train_to)

data_analyze = pd.merge(data_analyze_lms, data_analyze_los, on='SO_HD', how='left').drop_duplicates()
data_analyze = pd.merge(data_analyze, data_analyze_lstd, on='SO_HD', how='left').drop_duplicates()

print('data_analyze: ' + str(data_analyze.shape))

# check missing data
df_missing_data = check_df_nan(data_analyze)

data_analyze = data_analyze.drop(list(df_missing_data[df_missing_data['Missing Ratio'] >= 70].columns), axis=1)

# corr df
for col in data_analyze.columns:
    kde_target(col, data_analyze)

corrmat = data_analyze.corr()
f, ax = plt.subplots(figsize=(12, 9))
sns.heatmap(corrmat, vmax=.8, square=True)

# Distribution of feature
plt.figure(figsize=(12, 20))
for i, feature in enumerate([col for col in data_analyze.columns if col != 'label']):
    # create a new subplot for each source
    plt.subplot(4, 1, i + 1)
    # plot Good
    sns.kdeplot(data_analyze.loc[data_analyze['label'] == 'good', feature], label='target == good')
    # plot Bad
    sns.kdeplot(data_analyze.loc[data_analyze['label'] == 'bad', feature], label='target == bad')

    # Label the plots
    plt.title('Distribution of %s by Target Value' % feature)
    plt.xlabel('%s' % feature);
    plt.ylabel('Density');

plt.tight_layout(h_pad=2.5)

# filter out outlier
data_analyze = remove_outliers_using_quantiles(data_analyze, 'label', 'inner')

# impute data nan with random forest
it = IterativeImputer(estimator=RandomForestRegressor(random_state=42))
data_analyze = pd.DataFrame(it.fit_transform(data_analyze))
print(check_df_nan(data_analyze))

# LabelEncoder
lbl = preprocessing.LabelEncoder()
for x in data_analyze:
    try:
        data_analyze[x] = data_analyze[x].astype('float')
    except:
        lbl.fit(list(data_analyze[x].values))
        data_analyze[x] = lbl.transform(list(data_analyze[x].values))

# select feature importance
model_feature = lgb.LGBMClassifier(objective='binary', boosting_type='goss', n_estimators=10000,
                                   class_weight='balanced')

for i in range(2):
    # Split into training and validation set
    train_features, valid_features, train_y, valid_y = train_test_split(data_analyze, data_analyze['label'],
                                                                        test_size=0.25, random_state=i)

    # Train using early stopping
    model_feature.fit(train_features, train_y, early_stopping_rounds=100, eval_set=[(valid_features, valid_y)],
                      eval_metric='auc', verbose=200)

    # Record the feature importances
    feature_importances += model.feature_importances_

feature_importances = feature_importances / 2
feature_importances = pd.DataFrame({'feature': list(data_analyze.columns), 'importance': feature_importances}).sort_values(
    'importance', ascending=False)

print(feature_importances)

fea_train = ['...']


