import os
import datetime
import pandas as pd
import warnings
import matplotlib.pyplot as plt
warnings.filterwarnings('ignore')
import numpy as np

def take_last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
    return next_month - datetime.timedelta(days=next_month.day)

def last_day_month_for_train():
    list_last_day = []
    for month in range(1, 13):
        for year in range(2017, 2022):
            last_day_datetime = take_last_day_of_month(datetime.date(year, month, 1))
            last_day = last_day_datetime.strftime("%Y") + last_day_datetime.strftime(
                "%m") + last_day_datetime.strftime(
                "%d")
            list_last_day.append(last_day)
    return list_last_day


def process_data_lms(BAD_DPD_MAX, dt_train_frm, dt_train_to, max_emi):
    os.chdir(r'...')
    path = os.getcwd()
    files = os.listdir(path)
    process_data = 'process_lms_' + '_' + str(dt_train_frm) + '_' + str(dt_train_to)
    feature = ['...']
    if process_data in files:
        data_analyze_lms = pd.read_pickle(process_data)
    else:
        os.chdir(r'...')
        ## process lms
        path = os.getcwd()
        files = os.listdir(path)
        list_last_day = last_day_month_for_train()
        frames = []
        for file in files:
            if file.split('_')[-1] in list_last_day:
                data = pd.read_pickle(file)
                try:
                    data = data[feature]
                except:
                    pass
                frames.append(data)
                print('process ' + file)

        data_analyze_lms = pd.concat(frames)
        os.chdir(r'...')
        data_analyze_lms.to_pickle(process_data)
        print('save file process lms done')

        # get G/B label
    data_analyze_lms = data_analyze_lms.groupby(['..', '..']).agg(
        dpd_ever_max=('...', 'max'),
        nhom_no_final=('...', 'max'),
        ngay_giai_ngan=('...', 'last')).reset_index()

    data_analyze_lms = data_analyze_lms.loc[data_analyze_lms.KY_TRA_NO_HIEN_TAI <= int(max_emi)]

    data_analyze_lms = data_analyze_lms.groupby(['SO_CIF']).agg(dpd_ever_max=('dpd_ever_max', 'max'),
                                                                nhom_no_final=('nhom_no_final', 'max'),
                                                                ngay_giai_ngan=('ngay_giai_ngan', 'last')).reset_index()

    data_analyze_lms['nhom_no_final'] = data_analyze_lms['nhom_no_final'].fillna(0)

    data_analyze_lms.loc[(data_analyze_lms['dpd_ever_max'] >= BAD_DPD_MAX) | (
            data_analyze_lms['nhom_no_final'] >= '....'), 'label'] = 'bad'
    data_analyze_lms.loc[
        (data_analyze_lms['dpd_ever_max'] == '...') & (data_analyze_lms['nhom_no_final'] < '.....'), 'label'] = 'good'
    data_analyze_lms = data_analyze_lms[data_analyze_lms['label'].isnull() == False]

    data_analyze_lms = data_analyze_lms[['...']]

    data_analyze_lms = data_analyze_lms[data_analyze_lms['label'].isnull() == False]
    return data_analyze_lms


def process_data_los(dt_train_frm, dt_train_to):
    os.chdir(r'...')
    path = os.getcwd()
    files = os.listdir(path)
    feature_los = ['...']
    process_data = 'process_los_' + '_' + str(dt_train_frm) + '_' + str(dt_train_to)
    frames = []
    for file in files:
        data = pd.read_pickle(file)
        data = data[data['...'].isnull() == False]

        data = data[feature_los]
        frames.append(data)
        print('process ' + file)

    data_analyze_los = pd.concat(frames)
    data_analyze_los = pd.DataFrame(data_analyze_los)

    os.chdir(r'...')
    data_analyze_los.to_pickle(process_data)
    print('save file process los done')
    return data_analyze_los


def process_lstd(dt_train_frm, dt_train_to):
    os.chdir(r'...')
    path = os.getcwd()
    files = os.listdir(path)
    process_data = 'process_lstd_' + str(dt_train_frm) + '_' + str(dt_train_to)
    day_lstd_max = []
    for file in files:
        day_lstd_max.append(file.split('_')[-1])

    process_data_lstd = pd.read_pickle('LOS_QTRR_FIELD_LSTD_' + str(day_lstd_max[-1]))
    os.chdir(r'...')
    process_data_lstd.to_pickle(process_data)
    return process_data_lstd


def check_df_nan(df):
    all_data_na = (df.isnull().sum() / len(df)) * 100
    all_data_na = all_data_na.drop(all_data_na[all_data_na == 0].index).sort_values(ascending=False)[:30]
    missing_data = pd.DataFrame({'Missing Ratio': all_data_na}).reset_index()
    return missing_data


def kde_target(fea, df):
    # Calculate the correlation coefficient between the new variable and the target
    corr = df['label'].corr(df[fea])

    # Calculate medians for good and bad
    avg_good = df.ix[df['label'] == 'good', fea].median()
    avg_bad = df.ix[df['label'] == 'bad', fea].median()

    plt.figure(figsize=(12, 6))

    # Plot the distribution for label
    sns.kdeplot(df.ix[df['label'] == 'good', fea], label='TARGET == good')
    sns.kdeplot(df.ix[df['label'] == 'bad', fea], label='TARGET == bad)

    # label the plot
    plt.xlabel(fea);
    plt.ylabel('Density');
    plt.title('%s Distribution' % fea)
    plt.legend();

    # print out the correlation
    print('The correlation between %s and the label is %0.4f' % (fea, corr))
    # Print out average values
    print('Median value of good = %0.4f' % avg_good)
    print('Median value of bad =     %0.4f' % avg_bad)


def remove_outliers_using_quantiles(df, fea, qu_fence):
    a = df[fea].describe()

    iqr = a["75%"] - a["25%"]
    print("interquartile range:", iqr)

    upper_inner_fence = a["75%"] + 1.5 * iqr
    lower_inner_fence = a["25%"] - 1.5 * iqr
    print("upper_inner_fence:", upper_inner_fence)
    print("lower_inner_fence:", lower_inner_fence)

    upper_outer_fence = a["75%"] + 3 * iqr
    lower_outer_fence = a["25%"] - 3 * iqr
    print("upper_outer_fence:", upper_outer_fence)
    print("lower_outer_fence:", lower_outer_fence)

    count_over_upper = len(df[df[fea] > upper_inner_fence])
    count_under_lower = len(df[df[fea] < lower_inner_fence])
    percentage = 100 * (count_under_lower + count_over_upper) / a["count"]
    print("percentage of records out of inner fences: %.2f" % (percentage))

    count_over_upper = len(df[df[fea] > upper_outer_fence])
    count_under_lower = len(df[df[fea] < lower_outer_fence])
    percentage = 100 * (count_under_lower + count_over_upper) / a["count"]
    print("percentage of records out of outer fences: %.2f" % (percentage))

    if qu_fence == "inner":
        output_dataset = df[df[fea] <= upper_inner_fence]
        output_dataset = output_dataset[output_dataset[fea] >= lower_inner_fence]
    elif qu_fence == "outer":
        output_dataset = df[df[fea] <= upper_outer_fence]
        output_dataset = output_dataset[output_dataset[fea] >= lower_outer_fence]
    else:
        output_dataset = df

    print("length of input dataframe:", len(df))
    print("length of new dataframe after outlier removal:", len(output_dataset))

    return output_dataset


def _KM(y_pred, n_bins):
    _, thresholds = pd.qcut(y_pred, q=n_bins, retbins=True)
    cmd_BAD = []
    cmd_GOOD = []
    BAD_id = set(np.where(y_valid.apply(lambda x: 1 if x == 'good' else 0) == 0)[0])
    GOOD_id = set(np.where(y_valid.apply(lambda x: 1 if x == 'good' else 0) == 1)[0])
    total_BAD = len(BAD_id)
    total_GOOD = len(GOOD_id)
    for thres in thresholds:
        pred_id = set(np.where(y_pred <= thres)[0])
        # Đếm % số lượng hồ sơ BAD có xác suất dự báo nhỏ hơn hoặc bằng thres
        per_BAD = len(pred_id.intersection(BAD_id)) / total_BAD
        cmd_BAD.append(per_BAD)
        # Đếm % số lượng hồ sơ GOOD có xác suất dự báo nhỏ hơn hoặc bằng thres
        per_GOOD = len(pred_id.intersection(GOOD_id)) / total_GOOD
        cmd_GOOD.append(per_GOOD)
    cmd_BAD = np.array(cmd_BAD)
    cmd_GOOD = np.array(cmd_GOOD)
    return cmd_BAD, cmd_GOOD, thresholds
